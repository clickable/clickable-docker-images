#!/bin/bash

TARGET_ARCH=${TARGET_ARCH:-armhf}
TAG=${TAG:-latest}
TARGET_TAG=${TARGET_TAG:-latest}
BRANCH=${BRANCH:-dev}

docker build -t clickable/ci-$BRANCH-20.04-$TARGET_ARCH:$TAG \
    --build-arg TARGET_ARCH=$TARGET_ARCH \
    --build-arg TARGET_TAG=$TARGET_TAG \
    --build-arg BRANCH=$BRANCH \
    . \
    $@
