#!/bin/bash

UT_VERSION=${UT_VERSION:-next}
TARGET_ARCH=${TARGET_ARCH:-armhf}
TAG=${TAG:-latest}
TARGET_TAG=${TARGET_TAG:-latest}
BRANCH=${BRANCH:-dev}

docker build -t "clickable/ci-${BRANCH}-ut${UT_VERSION}-${TARGET_ARCH}:${TAG}" \
    --build-arg UT_VERSION="$UT_VERSION" \
    --build-arg TARGET_ARCH="$TARGET_ARCH" \
    --build-arg TARGET_TAG="$TARGET_TAG" \
    --build-arg BRANCH="$BRANCH" \
    . \
    "$@"
