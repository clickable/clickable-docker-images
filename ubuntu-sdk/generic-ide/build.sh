#!/bin/bash

UT_VERSION=${UT_VERSION:-next}
HOST_ARCH=${HOST_ARCH:-amd64}
TAG=${TAG:-latest}

docker build -t "clickable/${HOST_ARCH}-ut${UT_VERSION}-${HOST_ARCH}-ide:${TAG}" \
    --build-arg HOST_ARCH=$HOST_ARCH \
    --build-arg UT_VERSION="$UT_VERSION" \
    .
