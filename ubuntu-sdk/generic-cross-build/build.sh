#!/bin/bash

UT_VERSION=${UT_VERSION:-next}

. "$(dirname "$0")/../set-ut-version-info.sh"

HOST_ARCH=${HOST_ARCH:-amd64}
TARGET_ARCH=${TARGET_ARCH:-arm64}
TAG=${TAG:-latest}

if [ "$HOST_ARCH" = "armhf" ]; then
    DOCKER_PLATFORM=linux/arm32/v7
elif [ "$HOST_ARCH" = "arm64" ]; then
    DOCKER_PLATFORM=linux/arm64/v8
elif [ "$HOST_ARCH" = "amd64" ]; then
    DOCKER_PLATFORM=linux/amd64
else
    echo "Unsupported HOST_ARCH: $HOST_ARCH"
    exit 1
fi

if [ $TARGET_ARCH = "armhf" ]; then
    TARGET_ARCH_TRIPLET=arm-linux-gnueabihf
    RUST_ARCH=armv7-unknown-linux-gnueabihf
    TARGET_CPU=arm
    TARGET_ARCH_BITS=32
elif [ $TARGET_ARCH = "arm64" ]; then
    TARGET_ARCH_TRIPLET=aarch64-linux-gnu
    RUST_ARCH=aarch64-unknown-linux-gnu
    TARGET_CPU=arm64
    TARGET_ARCH_BITS=64
else
    echo "Unsupported TARGET_ARCH: $TARGET_ARCH"
    exit 1
fi

docker build -t "clickable/${HOST_ARCH}-ut${UT_VERSION}-${TARGET_ARCH}:${TAG}" \
    --build-arg UT_VERSION="$UT_VERSION" \
    --build-arg UT_BASE="$UT_BASE" \
    --build-arg UT_ARCHIVE_NAME="$UT_ARCHIVE_NAME" \
    --build-arg TARGET_ARCH=$TARGET_ARCH \
    --build-arg HOST_ARCH=$HOST_ARCH \
    --build-arg TARGET_ARCH_TRIPLET=$TARGET_ARCH_TRIPLET \
    --build-arg RUST_ARCH=$RUST_ARCH \
    --build-arg TARGET_CPU=$TARGET_CPU \
    --platform="$DOCKER_PLATFORM" \
    .
