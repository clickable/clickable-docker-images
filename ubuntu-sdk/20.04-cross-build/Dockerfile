FROM ubuntu:focal

ARG HOST_ARCH
ARG TARGET_ARCH
ARG TARGET_ARCH_TRIPLET
ARG TARGET_CPU
ARG RUST_ARCH
ARG CMAKE_ARCH
ARG IMAGE_VERSION=11

LABEL maintainer="Clickable Team"
LABEL image_version="$IMAGE_VERSION"

RUN echo set debconf/frontend Noninteractive | debconf-communicate && \
    echo set debconf/priority critical | debconf-communicate

# Add ubport repos
RUN echo "deb [arch=$HOST_ARCH] http://archive.ubuntu.com/ubuntu focal main restricted multiverse universe" > /etc/apt/sources.list && \
    echo "deb [arch=$HOST_ARCH] http://archive.ubuntu.com/ubuntu focal-updates main restricted multiverse universe" >> /etc/apt/sources.list && \
    echo "deb [arch=$HOST_ARCH] http://archive.ubuntu.com/ubuntu focal-security main restricted multiverse universe" >> /etc/apt/sources.list && \
    echo "deb [arch=$TARGET_ARCH] http://ports.ubuntu.com/ubuntu-ports focal main restricted multiverse universe" >> /etc/apt/sources.list && \
    echo "deb [arch=$TARGET_ARCH] http://ports.ubuntu.com/ubuntu-ports focal-updates main restricted multiverse universe" >> /etc/apt/sources.list && \
    echo "deb [arch=$TARGET_ARCH] http://ports.ubuntu.com/ubuntu-ports focal-security main restricted multiverse universe" >> /etc/apt/sources.list && \
    # Allow installing cross-platform packages
    dpkg --add-architecture $TARGET_ARCH && \
    # Add other repos
    apt-get update && \
    apt-get -y -f --no-install-recommends install gnupg ubuntu-keyring software-properties-common wget && \
    echo "deb http://repo2.ubports.com focal main" >> /etc/apt/sources.list && \
    wget -q https://repo.ubports.com/keyring.gpg -O /etc/apt/trusted.gpg.d/ubports.gpg && \
    add-apt-repository ppa:bhdouglass/clickable && \
    add-apt-repository ppa:mardy/qbs-on-lts && \
    # Cleanup
    apt-get -y autoremove && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install dependencies
RUN apt-get update && \
    apt-get -y --no-install-recommends dist-upgrade && \
    apt-get -y --no-install-recommends install \
    build-essential \
    pkg-config \
    git \
    dpkg-cross \
    g++-$TARGET_ARCH_TRIPLET \
    crossbuild-essential-$TARGET_ARCH \
    gdb-multiarch \
    gdbserver:$TARGET_ARCH \
    libc6-dbg:$TARGET_ARCH \
    libc-dev:$TARGET_ARCH \
    libicu-dev:$TARGET_ARCH \
    pkg-config-$TARGET_ARCH_TRIPLET \
    qbs \
    qtbase5-private-dev:$TARGET_ARCH \
    qtdeclarative5-private-dev:$TARGET_ARCH \
    qtfeedback5-dev:$TARGET_ARCH \
    qtpositioning5-dev:$TARGET_ARCH \
    qtquickcontrols2-5-dev:$TARGET_ARCH \
    qtsystems5-dev:$TARGET_ARCH \
    qtwebengine5-dev:$TARGET_ARCH \
    libqt5opengl5-dev:$TARGET_ARCH \
    click \
    click-reviewers-tools \
    ubuntu-sdk-libs-dev:$TARGET_ARCH \
    ubuntu-sdk-libs-tools \
    ubuntu-sdk-libs:$TARGET_ARCH \
    liblomiri-connectivity-qt1-dev:$TARGET_ARCH \
    libnotify-dev:$TARGET_ARCH \
    libtag1-dev:$TARGET_ARCH \
    libpam0g-dev:$TARGET_ARCH \
    python3-requests \
    python3-gnupg \
    wget \
    libgles2 \
    libasound2:$TARGET_ARCH \
    libfreetype6:$TARGET_ARCH \
    libglib2.0-0:$TARGET_ARCH \
    libpng16-16:$TARGET_ARCH \
    libsdl2-2.0-0:$TARGET_ARCH \
    libvpx6:$TARGET_ARCH \
    libsndio7.0:$TARGET_ARCH \
    curl \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Go env vars
ENV PATH=/usr/local/go/bin/:$PATH

# Install Go
RUN wget https://dl.google.com/go/go1.16.14.linux-$HOST_ARCH.tar.gz && \
    tar -xf go*.linux-$HOST_ARCH.tar.gz && \
    mv go /usr/local && \
    rm go*.linux-$HOST_ARCH.tar.gz

# Work around weird Go linker issue https://gitlab.com/clickable/clickable/-/issues/251
RUN ln -fs /usr/include/$TARGET_ARCH_TRIPLET/qt5/QtCore/5.12.8/QtCore/ /usr/include/QtCore

# Rust env vars
ENV CARGO_HOME=/opt/rust/cargo \
    RUSTUP_HOME=/opt/rust/rustup \
    PATH=/opt/rust/cargo/bin:$PATH \
    CLICKABLE_RUST_CHANNEL=1.72.1

# Install Rust
RUN mkdir -p /opt/rust && \
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs \
        | bash -s -- -y --default-toolchain $CLICKABLE_RUST_CHANNEL && \
    rustup target add $RUST_ARCH && \
    # Allow the default clickable user to update the registry as well as the git folder
    mkdir -p /opt/rust/cargo/registry && \
    chown -R 1000 /opt/rust/cargo/registry && \
    mkdir -p /opt/rust/cargo/git && \
    chown -R 1000 /opt/rust/cargo/git && \
    chown -R 1000 /opt/rust/rustup/tmp

# TODO see if there is a better location for this
ADD qt-$TARGET_ARCH.conf /usr/bin/qt.conf
ADD qt-$TARGET_ARCH.conf /usr/local/bin/qt.conf

# QMake
## See https://gitlab.com/ubports/development/core/focal-projectmanagement-missing-packages/-/issues/72
ADD ubuntu-click-tools.prf /usr/lib/$TARGET_ARCH_TRIPLET/qt5/mkspecs/features/ubuntu-click-tools.prf
ADD ubuntu-click.prf /usr/lib/$TARGET_ARCH_TRIPLET/qt5/mkspecs/features/ubuntu-click.prf

## Fix Rust and Go qmake access
RUN ln -sf /usr/lib/$TARGET_ARCH_TRIPLET/qt5/bin/qmake /usr/local/bin/qmake

# QBS setup
RUN qbs setup-toolchains --system --detect && \
    qbs setup-qt --system /usr/local/bin/qmake qt5 && \
    qbs config --system defaultProfile qt5 && \
    qbs config --system profiles.qt5.baseProfile $TARGET_ARCH_TRIPLET-gcc-9

# Download and store UT-compatible SDL
RUN mkdir /opt/sdl && \
        curl https://gitlab.com/clickable/godot-patched/-/jobs/artifacts/main/download?job=sdl_${TARGET_ARCH} -L -o sdl.zip && \
        unzip sdl.zip && \
        mv build/${TARGET_ARCH_TRIPLET}/sdl/install/* /opt/sdl/ && \
        rm -r sdl.zip build

# Download and store UT-compatible Godot
RUN mkdir /opt/godot && \
        curl https://gitlab.com/clickable/godot-patched/-/jobs/artifacts/main/download?job=godot_${TARGET_ARCH} -L -o godot.zip && \
        unzip godot.zip && \
        mv build/${TARGET_ARCH_TRIPLET}/godot/install/godot /opt/godot/godot && \
        rm -r godot.zip build

# Install CMake
RUN if [ -z "$CMAKE_ARCH" ]; then \
        apt-get update && \
        apt-get -y --no-install-recommends install cmake && \
        apt-get clean && \
        rm -rf /var/lib/apt/lists/*; \
    else \
        mkdir /opt/cmake && \
        wget https://github.com/Kitware/CMake/releases/download/v3.22.3/cmake-3.22.3-linux-$CMAKE_ARCH.sh -O cmake-installer.sh && \
        sh cmake-installer.sh --skip-license --prefix=/opt/cmake && \
        rm cmake-installer.sh; \
    fi
ENV PATH=/opt/cmake/bin:$PATH

# Build and install sed 4.8 in order to fix https://gitlab.com/clickable/clickable/-/issues/414
RUN apt-get update && apt-get install -y autopoint texinfo rsync && \
    git clone --branch v4.8 --depth=1 --recurse-submodules --shallow-submodules \
        git://git.sv.gnu.org/sed && \
    cd sed && \
    ./bootstrap && ./configure --disable-dependency-tracking --disable-i18n && \
    make -j8 && mkdir -p /opt/sed && mv sed/sed /opt/sed/ && \
    cd - &&  rm -rf sed && \
    apt-get remove -y autopoint texinfo rsync &&  rm -rf /var/lib/apt/lists/*
ENV PATH=/opt/sed:$PATH

# Generated from `dpkg-architecture -a armhf`
ENV DEB_BUILD_ARCH=amd64 \
    DEB_BUILD_ARCH_BITS=64 \
    DEB_BUILD_ARCH_CPU=amd64 \
    DEB_BUILD_ARCH_ENDIAN=little \
    DEB_BUILD_ARCH_OS=linux \
    DEB_BUILD_GNU_CPU=x86_64 \
    DEB_BUILD_GNU_SYSTEM=linux-gnu \
    DEB_BUILD_GNU_TYPE=x86_64-linux-gnu \
    DEB_BUILD_MULTIARCH=. \
    DEB_HOST_ARCH=$TARGET_ARCH \
    DEB_HOST_ARCH_BITS=$TARGET_ARCH_BITS \
    DEB_HOST_ARCH_CPU=$TARGET_CPU \
    DEB_HOST_ARCH_ENDIAN=little \
    DEB_HOST_ARCH_OS=linux \
    DEB_HOST_GNU_CPU=$TARGET_CPU \
    DEB_HOST_GNU_SYSTEM=linux-gnueabihf \
    DEB_HOST_GNU_TYPE=$TARGET_ARCH_TRIPLET \
    DEB_HOST_MULTIARCH=$TARGET_ARCH_TRIPLET \
    DEB_TARGET_ARCH=$TARGET_ARCH \
    DEB_TARGET_ARCH_BITS=$TARGET_ARCH_BITS \
    DEB_TARGET_ARCH_CPU=$TARGET_CPU \
    DEB_TARGET_ARCH_ENDIAN=little \
    DEB_TARGET_ARCH_OS=linux \
    DEB_TARGET_GNU_CPU=$TARGET_CPU \
    DEB_TARGET_GNU_SYSTEM=linux-gnueabihf \
    DEB_TARGET_GNU_TYPE=$TARGET_ARCH_TRIPLET \
    DEB_TARGET_MULTIARCH=$TARGET_ARCH_TRIPLET \
    # Env vars for Go cross compile
    GOOS=linux \
    GOARCH=$TARGET_CPU \
    CGO_ENABLED=1 \
    PKG_CONFIG_LIBDIR=/usr/lib/$TARGET_ARCH_TRIPLET/pkgconfig:/usr/lib/pkgconfig:/usr/share/pkgconfig \
    CC=$TARGET_ARCH_TRIPLET-gcc \
    CXX=$TARGET_ARCH_TRIPLET-g++ \
    CARGO_TARGET_ARMV7_UNKNOWN_LINUX_GNUEABIHF_LINKER=arm-linux-gnueabihf-gcc \
    CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER=aarch64-linux-gnu-gcc \
    CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_LINKER=x86_64-linux-gnu-gcc \
    PKG_CONFIG_ALLOW_CROSS=1
