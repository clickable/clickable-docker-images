#!/bin/bash

UT_VERSION=${UT_VERSION:-next}

. "$(dirname "$0")/../set-ut-version-info.sh"

if [ "$UT_BASE" = "noble" ]; then
    # FIXME: nvidia/opengl Docker image doesn't have 24.04 images yet.
    NVIDIA_UBUNTU_BASE=22.04
else
    NVIDIA_UBUNTU_BASE=$(ubuntu-distro-info --series="$UT_BASE" --release|cut -d' ' -f1)
fi

HOST_ARCH=${HOST_ARCH:-amd64}
TAG=${TAG:-latest}
# Set to "-ide" to build the nvidia-ide image
IDE=${IDE:-""}

docker build -t "clickable/${HOST_ARCH}-ut${UT_VERSION}-${HOST_ARCH}-nvidia${IDE}:${TAG}" \
    --build-arg HOST_ARCH=$HOST_ARCH \
    --build-arg UT_VERSION="$UT_VERSION" \
    --build-arg NVIDIA_UBUNTU_BASE="$NVIDIA_UBUNTU_BASE" \
    --build-arg IDE=$IDE \
    .
