FROM ubuntu:focal

ARG HOST_ARCH
ARG HOST_ARCH_TRIPLET
ARG QEMU_USER_STATIC_ARCH
ARG DEB_URL
ARG QBS_SUPPORT
ARG GO_ARCH
ARG CMAKE_ARCH
ARG IMAGE_VERSION=11

LABEL maintainer="Clickable Team"
LABEL image_version="$IMAGE_VERSION"

COPY qemu-$QEMU_USER_STATIC_ARCH-static /usr/bin

RUN echo set debconf/frontend Noninteractive | debconf-communicate && \
    echo set debconf/priority critical | debconf-communicate

# Add ubport repos
RUN echo "deb [arch=$HOST_ARCH] $DEB_URL focal main restricted multiverse universe" > /etc/apt/sources.list && \
    echo "deb [arch=$HOST_ARCH] $DEB_URL focal-updates main restricted multiverse universe" >> /etc/apt/sources.list && \
    echo "deb [arch=$HOST_ARCH] $DEB_URL focal-security main restricted multiverse universe" >> /etc/apt/sources.list && \
    # Add other repos
    apt-get update && \
    apt-get -y -f --no-install-recommends install gnupg ubuntu-keyring software-properties-common wget && \
    echo "deb http://repo2.ubports.com focal main" >> /etc/apt/sources.list && \
    wget -q https://repo.ubports.com/keyring.gpg -O /etc/apt/trusted.gpg.d/ubports.gpg && \
    add-apt-repository ppa:bhdouglass/clickable && \
    add-apt-repository ppa:mardy/qbs-on-lts && \
    # Cleanup
    apt-get -y autoremove && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install dependencies
RUN apt-get update && \
    apt-get -y --no-install-recommends dist-upgrade && \
    apt-get -y --no-install-recommends install \
        $QBS_SUPPORT \
        build-essential \
        click \
        click-reviewers-tools \
        curl \
        dbus-x11 \
        dconf-service \
        g++ \
        gdb \
        gdbserver \
        git \
        gstreamer1.0-plugins-bad \
        gstreamer1.0-plugins-good \
        gstreamer1.0-plugins-ugly \
        isc-dhcp-client \
        language-pack-en \
        libandroid-properties-dev \
        libandroid-properties1 \
        libasound2 \
        libc-dev \
        libfreetype6 \
        libgles2-mesa \
        libglib2.0-0 \
        libglib2.0-dev \
        libicu-dev \
        liblomiri-connectivity-qt1-dev \
        libmission-control-plugins-dev \
        libmission-control-plugins0 \
        libnotify-dev \
        libofono-qt-dev \
        libpam0g-dev \
        libphonenumber-dev \
        libpng16-16 \
        libqofono-dev \
        libqt5gstreamer-dev \
        libqt5opengl5-dev \
        libsdl2-2.0-0 \
        libsmbclient-dev \
        libsndio7.0 \
        libsqlite3-dev \
        libtag1-dev \
        libtelepathy-qt5-dev \
        libvpx6 \
        locales-all \
        morph-browser \
        morph-webapp-container \
        pkg-config \
        python3-gnupg \
        python3-requests \
        qml-module-gsettings1.0 \
        qml-module-io-thp-pyotherside \
        qml-module-lomiri-addressbook0.1 \
        qml-module-lomiri-connectivity \
        qml-module-lomiri-contacts0.1 \
        qml-module-lomiri-history0.1 \
        qml-module-lomiri-keyboard \
        qml-module-lomiri-notifications \
        qml-module-lomiri-telephony-phonenumber0.1 \
        qml-module-lomiri-telephony0.1 \
        qml-module-lomiri-thumbnailer0.1 \
        qml-module-morph-web \
        qml-module-ofono \
        qml-module-qt-labs-folderlistmodel \
        qml-module-qt-labs-platform \
        qml-module-qt-labs-settings \
        qml-module-qtcontacts \
        qml-module-qtquick-controls2-suru \
        qml-module-qtquick-dialogs \
        qtbase5-private-dev \
        qtdeclarative5-private-dev \
        qtfeedback5-dev \
        qtpositioning5-dev \
        qtquickcontrols2-5-dev \
        qtsystems5-dev \
        qtwebengine5-dev \
        telepathy-mission-control-5 \
        tzdata \
        ubuntu-mobile-icons \
        ubuntu-sdk-libs \
        ubuntu-sdk-libs-dev \
        ubuntu-sdk-libs-tools \
        valgrind \
        wget \
        xdg-utils \
        xvfb \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Let apps in Desktop Mode open links in Morph
RUN xdg-settings set default-web-browser morph-browser.desktop

# Go env vars
ENV PATH=/usr/local/go/bin/:$PATH

# Install Go
RUN wget https://dl.google.com/go/go1.16.14.linux-$GO_ARCH.tar.gz && \
    tar -xf go*.linux-$GO_ARCH.tar.gz && \
    mv go /usr/local && \
    rm go*.linux-$GO_ARCH.tar.gz

# Work around weird Go linker issue https://gitlab.com/clickable/clickable/-/issues/251
RUN ln -fs /usr/include/$HOST_ARCH_TRIPLET/qt5/QtCore/5.12.8/QtCore/ /usr/include/QtCore

# Rust env vars
ENV CARGO_HOME=/opt/rust/cargo \
    RUSTUP_HOME=/opt/rust/rustup \
    PATH=/opt/rust/cargo/bin:$PATH \
    CLICKABLE_RUST_CHANNEL=1.72.1

# Install Rust
RUN mkdir -p /opt/rust && \
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs \
        | bash -s -- -y --default-toolchain $CLICKABLE_RUST_CHANNEL && \
    # Allow the default clickable user to update the registry as well as the git folder
    mkdir -p /opt/rust/cargo/registry && \
    chown -R 1000 /opt/rust/cargo/registry && \
    mkdir -p /opt/rust/cargo/git && \
    chown -R 1000 /opt/rust/cargo/git && \
    chown -R 1000 /opt/rust/rustup/tmp

# QMake
## See https://gitlab.com/ubports/development/core/focal-projectmanagement-missing-packages/-/issues/72
ADD ubuntu-click-tools.prf /usr/lib/$HOST_ARCH_TRIPLET/qt5/mkspecs/features/ubuntu-click-tools.prf
ADD ubuntu-click.prf /usr/lib/$HOST_ARCH_TRIPLET/qt5/mkspecs/features/ubuntu-click.prf

# QBS setup
RUN if [ $QBS_SUPPORT == "qbs" ]; then \
        qbs setup-toolchains --system --detect && \
        qbs setup-qt --system /usr/bin/qmake qt5 && \
        qbs config --system defaultProfile qt5 && \
        qbs config --system profiles.qt5.baseProfile gcc; \
    fi

# Download and store UT-compatible SDL
RUN mkdir /opt/sdl && \
        curl https://gitlab.com/clickable/godot-patched/-/jobs/artifacts/main/download?job=sdl_${HOST_ARCH} -L -o sdl.zip && \
        unzip sdl.zip && \
        mv build/${HOST_ARCH_TRIPLET}/sdl/install/* /opt/sdl/ && \
        rm -r sdl.zip build

# Download and store UT-compatible Godot
RUN mkdir /opt/godot && \
        curl https://gitlab.com/clickable/godot-patched/-/jobs/artifacts/main/download?job=godot_${HOST_ARCH} -L -o godot.zip && \
        unzip godot.zip && \
        mv build/${HOST_ARCH_TRIPLET}/godot/install/godot /opt/godot/godot && \
        rm -r godot.zip build

# Install CMake
RUN if [ -z "$CMAKE_ARCH" ]; then \
        apt-get update && \
        apt-get -y --no-install-recommends install cmake && \
        apt-get clean && \
        rm -rf /var/lib/apt/lists/*; \
    else \
        mkdir /opt/cmake && \
        wget https://github.com/Kitware/CMake/releases/download/v3.22.3/cmake-3.22.3-linux-$CMAKE_ARCH.sh -O cmake-installer.sh && \
        sh cmake-installer.sh --skip-license --prefix=/opt/cmake && \
        rm cmake-installer.sh; \
    fi
ENV PATH=/opt/cmake/bin:$PATH

# Install qmlscene alternative qmllive, that allows for live update
RUN git clone https://github.com/penk/qml-livereload.git && \
    cd qml-livereload && qmake && make && \
    cd - && rm -rf qml-livereload

# Build and install sed 4.8 in order to fix https://gitlab.com/clickable/clickable/-/issues/414
RUN apt-get update && apt-get install -y autopoint texinfo rsync && \
    git clone --branch v4.8 --depth=1 --recurse-submodules --shallow-submodules \
        git://git.sv.gnu.org/sed && \
    cd sed && \
    ./bootstrap && ./configure --disable-dependency-tracking --disable-i18n && \
    make -j8 && mkdir -p /opt/sed && mv sed/sed /opt/sed/ && \
    cd - &&  rm -rf sed && \
    apt-get remove -y autopoint texinfo rsync &&  rm -rf /var/lib/apt/lists/*
ENV PATH=/opt/sed:$PATH

# Add user phablet
RUN groupadd -r phablet -g 1000 && useradd -u 1000 -r -g phablet -m -d /home/phablet -s /sbin/nologin -c "phablet user"  phablet && \
    chmod 755 /home/phablet

ADD xvfb-startup.sh /usr/local/bin/xvfb-startup
RUN chmod +x /usr/local/bin/xvfb-startup

ENV CGO_ENABLED=1
ENV QT_QPA_PLATFORMTHEME=ubuntuappmenu
