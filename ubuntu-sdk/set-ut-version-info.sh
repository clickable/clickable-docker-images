# shellcheck shell=bash

if [ -z "$UT_VERSION" ]; then
    echo "Error: \$UT_VERSION is required to be set."
    exit 1
fi

# TODO: maybe host this file somewhere else?
ut_version_url="https://gitlab.com/ubports/infrastructure/build-tools/-/raw/main/ut-version-info.json?ref_type=heads"
version_info=$(curl --silent --location "$ut_version_url" | jq --arg v "$UT_VERSION" '.[$v]')

# shellcheck disable=SC2034
UT_BASE=$(echo "$version_info" | jq -r .base_distro.release)
# shellcheck disable=SC2034
UT_ARCHIVE_NAME=$(echo "$version_info" | jq -r .aptly_archive)

unset ut_version_url version_info
