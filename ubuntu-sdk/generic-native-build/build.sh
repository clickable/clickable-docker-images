#!/bin/bash

UT_VERSION=${UT_VERSION:-next}

. "$(dirname "$0")/../set-ut-version-info.sh"

HOST_ARCH=${HOST_ARCH:-amd64}
TAG=${TAG:-latest}

if [ $HOST_ARCH = "armhf" ]; then
    HOST_ARCH_TRIPLET=arm-linux-gnueabihf
    DOCKER_PLATFORM=linux/arm32/v7
    QEMU_USER_STATIC_ARCH=arm
    QBS_SUPPORT=
    GO_ARCH=armv6l
elif [ $HOST_ARCH = "arm64" ]; then
    HOST_ARCH_TRIPLET=aarch64-linux-gnu
    DOCKER_PLATFORM=linux/arm64/v8
    QEMU_USER_STATIC_ARCH=aarch64
    QBS_SUPPORT=
    GO_ARCH=arm64
elif [ $HOST_ARCH = "amd64" ]; then
    HOST_ARCH_TRIPLET=x86_64-linux-gnu
    DOCKER_PLATFORM=linux/amd64
    QEMU_USER_STATIC_ARCH=x86_64
    QBS_SUPPORT=qbs
    GO_ARCH=amd64
else
    echo "Unsupported HOST_ARCH: $HOST_ARCH"
    exit 1
fi

# Reference: https://stackoverflow.com/a/54595564/6640061
QEMU_USER_STATIC_DOWNLOAD_URL="https://github.com/multiarch/qemu-user-static/releases/download"
QEMU_USER_STATIC_LATEST_TAG=$(curl -s https://api.github.com/repos/multiarch/qemu-user-static/tags \
    | grep 'name.*v[0-9]' \
    | head -n 1 \
    | cut -d '"' -f 4)

curl -SL "${QEMU_USER_STATIC_DOWNLOAD_URL}/${QEMU_USER_STATIC_LATEST_TAG}/x86_64_qemu-${QEMU_USER_STATIC_ARCH}-static.tar.gz" \
    | tar xzv

docker run --rm --privileged multiarch/qemu-user-static:register --reset

docker build -t "clickable/${HOST_ARCH}-ut${UT_VERSION}-${HOST_ARCH}:${TAG}" \
    --build-arg UT_VERSION="$UT_VERSION" \
    --build-arg UT_BASE="$UT_BASE" \
    --build-arg UT_ARCHIVE_NAME="$UT_ARCHIVE_NAME" \
    --build-arg HOST_ARCH_TRIPLET=$HOST_ARCH_TRIPLET \
    --build-arg HOST_ARCH=$HOST_ARCH \
    --build-arg QEMU_USER_STATIC_ARCH=$QEMU_USER_STATIC_ARCH \
    --build-arg QBS_SUPPORT=$QBS_SUPPORT \
    --build-arg GO_ARCH=$GO_ARCH \
    --platform="$DOCKER_PLATFORM" \
    .
